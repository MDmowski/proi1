//
// Created by Maciej Dmowski on 2019-03-02.
//

#ifndef PROJEKT1_FUNCTION_H
#define PROJEKT1_FUNCTION_H

#include <iostream>

using namespace std;

class Function{
    double a,b,c;

public:
    Function( double = 0, double = 0, double = 0 );

    void findRoots();

};


#endif //PROJEKT1_FUNCTION_H
