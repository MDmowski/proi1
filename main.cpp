//
// Created by Maciej Dmowski on 2019-03-02.
//
#include <iostream>
#include "function.h"

using namespace std;

int main( int argc, const char * argv[] ) {
    Function func;
    func.findRoots();
    return 0;
}
