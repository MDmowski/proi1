//
// Created by Maciej Dmowski on 2019-03-02.
//
#include <iostream>
#include "function.h"
#include <math.h>
#include <iomanip>
#include <cstdlib>


using namespace std;

Function::Function( double a1, double b1, double c1 ){
    cout << "Podaj wartosci parametrow funkcji: ax^2+bx+c"<<endl;
    cin >> a1 >> b1 >> c1;
    a = a1;
    b = b1;
    c = c1;
}

void Function::findRoots(){
    if( a == 0 ){
        cout << "Funkcja jest liniowa.\n"<<"Czy kontynuować? t - tak, n - nie"<<endl;
        char choice;
        cin>>choice;
        switch (choice){
            case 't':
                if(b != 0)
                cout<<"Miejsce zerowe wynosi: "<<-1*c/b<<endl;
                else{
                    if(c == 0)
                        cout<<"0 = 0 Nieskonczenie wiele rozwiazan"<<endl;
                    else
                        cout<<"Sprzecznosc! Brak miejsc zerowych"<<endl;
                }
                break;

            case 'n':
                exit(0);

            default:
                exit(0);

        }
    }else {
        double delta = b * b - 4 * a * c;
        if (delta < 0) {
            cout << "Brak pierwiastkow rzeczywistych\n";
        } else {
            double x1 = (-b - sqrt(delta)) / (2 * a);
            double x2 = (-b + sqrt(delta)) / (2 * a);
            if(x1 != x2)
                cout << setprecision(4)<< fixed << "Miejsca zerowe tej funkcji to: " << x1 << " i " << x2 <<endl;
            else
                cout << setprecision(4) << fixed << "Funkcja posiada jedno miejsce zerowe: " << x1 <<endl;
        }
    }
}

