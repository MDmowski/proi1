projekt1.o: main.o function.o
	g++ main.o function.o -o projekt1.o

main.o: main.cpp function.h
	g++ -c -Wall -pedantic main.cpp

function.o: function.cpp function.h
	g++ -c -Wall -pedantic function.cpp

clean:
	rm *.o

debug:
	g++ -g *.cpp -o debug.o
